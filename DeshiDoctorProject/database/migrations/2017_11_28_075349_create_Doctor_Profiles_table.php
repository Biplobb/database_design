<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Doctor_Profiles', function (Blueprint $table) {
            $table->increments('doctor_id');
            $table->string('name','25');
            $table->string('deprtment','25');
            $table->string('experience','25');
            $table->string('picture','255');

            $table->string('degree','25');
            $table->integer('specialist_id')->unsigned();

            $table->foreign('specialist_id')->references('id')->on('Specialist');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Doctor_Profiles');
    }
}
